package com.kvs.data.network;

import com.kvs.data.entity.CurrenciesEntity;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by kvs on 05.07.17.
 */

public interface FixerService {

    @GET("/latest")
    Observable<CurrenciesEntity> listOfCurrency();

    @GET("/latest")
    Observable<CurrenciesEntity> listOfCurrencyWithBase(@Query("base") String base);

}
