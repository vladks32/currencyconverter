package com.kvs.data.entity;

import java.util.Date;
import java.util.HashMap;

/**
 * Represents Currencies in data layer
 **/
public class CurrenciesEntity {

    private String base;
    private HashMap<String, Float> rates;
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public HashMap<String, Float> getRates() {
        return rates;
    }

    public void setRates(HashMap<String, Float> rates) {
        this.rates = rates;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }
}
