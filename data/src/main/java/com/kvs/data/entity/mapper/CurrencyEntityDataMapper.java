package com.kvs.data.entity.mapper;

import com.kvs.data.entity.CurrenciesEntity;
import com.kvs.entity.Currencies;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by kvs on 05.07.17.
 */
@Singleton
public class CurrencyEntityDataMapper {

    @Inject
    CurrencyEntityDataMapper() {
    }

    //transform data entity to domain entity(in this case it same, but usually may be different)
    public Currencies transform(CurrenciesEntity currenciesEntity) {
        Currencies currencies = new Currencies();
        currencies.setBase(currenciesEntity.getBase());
        currencies.setDate(currenciesEntity.getDate());
        currencies.setRates(currenciesEntity.getRates());
        return currencies;
    }

}
