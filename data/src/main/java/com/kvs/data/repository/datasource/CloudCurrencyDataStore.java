package com.kvs.data.repository.datasource;

import com.kvs.data.cache.CurrencyCache;
import com.kvs.data.entity.CurrenciesEntity;
import com.kvs.data.network.FixerService;

import io.reactivex.Observable;

/**
 * Created by kvs on 05.07.17.
 */

public class CloudCurrencyDataStore implements CurrencyDataStore {

    private final FixerService apiService;
    private final CurrencyCache currencyCache;

    CloudCurrencyDataStore(FixerService apiService, CurrencyCache currencyCache) {
        this.apiService = apiService;
        this.currencyCache = currencyCache;
    }

    @Override
    public Observable<CurrenciesEntity> currenciesEntity() {
        return apiService.listOfCurrency().doOnNext(currencyCache::put);
    }

    @Override
    public Observable<CurrenciesEntity> currenciesEntityWithBaseCurrency(String baseName) {
        return apiService.listOfCurrencyWithBase(baseName).doOnNext(currencyCache::put);
    }
}
