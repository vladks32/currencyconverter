package com.kvs.data.repository.datasource;

import com.kvs.data.entity.CurrenciesEntity;

import io.reactivex.Observable;

/**
 * Created by kvs on 05.07.17.
 */

public interface CurrencyDataStore {

    Observable<CurrenciesEntity> currenciesEntity();

    Observable<CurrenciesEntity> currenciesEntityWithBaseCurrency(final String baseName);

}
