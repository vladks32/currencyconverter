package com.kvs.data.repository.datasource;

import android.support.annotation.NonNull;

import com.kvs.data.cache.CurrencyCache;
import com.kvs.data.network.FixerService;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by kvs on 05.07.17.
 */
@Singleton
public class CurrencyDataStoreFactory {

    private final CurrencyCache currencyCache;
    private final FixerService apiService;

    @Inject
    CurrencyDataStoreFactory(@NonNull CurrencyCache currencyCache,
                             @NonNull FixerService apiService) {
        this.currencyCache = currencyCache;
        this.apiService = apiService;
    }


    public CurrencyDataStore create(final String baseName) {
        CurrencyDataStore currencyDataStore;

        if (!this.currencyCache.isExpired() && this.currencyCache.isCached(baseName)) {
            currencyDataStore = new DatabaseCurrencyDataStore();
        } else {
            currencyDataStore = createCloudDataStore();
        }
        return currencyDataStore;
    }

    public CurrencyDataStore createCloudDataStore() {
        return new CloudCurrencyDataStore(apiService, currencyCache);
    }
}
