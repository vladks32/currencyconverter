package com.kvs.data.repository.datasource;

import com.kvs.data.entity.CurrenciesEntity;

import io.reactivex.Observable;

/**
 * Created by kvs on 05.07.17.
 */

public class DatabaseCurrencyDataStore implements CurrencyDataStore {
    @Override
    public Observable<CurrenciesEntity> currenciesEntity() {
        return null;
    }

    @Override
    public Observable<CurrenciesEntity> currenciesEntityWithBaseCurrency(String baseName) {
        return null;
    }
}
