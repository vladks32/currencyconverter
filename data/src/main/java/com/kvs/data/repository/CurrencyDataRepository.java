package com.kvs.data.repository;

import com.kvs.data.entity.mapper.CurrencyEntityDataMapper;
import com.kvs.data.repository.datasource.CurrencyDataStore;
import com.kvs.data.repository.datasource.CurrencyDataStoreFactory;
import com.kvs.entity.Currencies;
import com.kvs.repository.CurrencyRepository;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by kvs on 05.07.17.
 */
@Singleton
public class CurrencyDataRepository implements CurrencyRepository {

    private final CurrencyDataStoreFactory currencyDataStoreFactory;
    private final CurrencyEntityDataMapper currencyEntityDataMapper;

    @Inject
    CurrencyDataRepository(CurrencyDataStoreFactory currencyDataStoreFactory, CurrencyEntityDataMapper currencyEntityDataMapper) {
        this.currencyDataStoreFactory = currencyDataStoreFactory;
        this.currencyEntityDataMapper = currencyEntityDataMapper;
    }

    @Override
    public Observable<Currencies> currencies() {
        CurrencyDataStore currencyDataStore = this.currencyDataStoreFactory.createCloudDataStore();
        return currencyDataStore.currenciesEntity().map(this.currencyEntityDataMapper::transform);
    }

    @Override
    public Observable<Currencies> currenciesWithBase(String baseName) {
        CurrencyDataStore currencyDataStore = this.currencyDataStoreFactory.create(baseName);
        return currencyDataStore.currenciesEntityWithBaseCurrency(baseName).map(this.currencyEntityDataMapper::transform);
    }

}
