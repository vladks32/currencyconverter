package com.kvs.data.cache;

import com.kvs.data.entity.CurrenciesEntity;

import io.reactivex.Observable;

/**
 * Created by kvs on 05.07.17.
 */

public interface CurrencyCache {

    Observable<CurrenciesEntity> get(final String baseName);

    void put(CurrenciesEntity currenciesEntity);

    boolean isCached(final String baseName);

    boolean isExpired();

    void clearCache();
}
