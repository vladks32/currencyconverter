package com.kvs.data.cache;

import android.content.Context;

import com.kvs.data.entity.CurrenciesEntity;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by kvs on 05.07.17.
 */

@Singleton
public class CurrencyCacheImpl implements CurrencyCache {

    private final Context context;

    /**
     * Possible implement cache, database, file or other ways
     **/
    @Inject
    CurrencyCacheImpl(Context context) {
        this.context = context;
    }

    @Override
    public Observable<CurrenciesEntity> get(String baseName) {
        return null;
    }

    @Override
    public void put(CurrenciesEntity currenciesEntity) {

    }

    @Override
    public boolean isCached(String baseName) {
        return false;
    }

    @Override
    public boolean isExpired() {
        return false;
    }

    @Override
    public void clearCache() {

    }
}
