package com.kvs.currencyconverter.di.components;

import android.app.Activity;


import com.kvs.currencyconverter.di.PerActivity;
import com.kvs.currencyconverter.di.modules.ActivityModule;

import dagger.Component;

/**
 * Created by kvs on 04.07.17.
 */

/**
 * A base component upon which fragment's components may depend.
 * Activity-level components should extend this component.
 *
 * Subtypes of ActivityComponent should be decorated with annotation: @PerActivity}
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
interface ActivityComponent {
    //Exposed to sub-graphs.
    Activity activity();
}
