package com.kvs.currencyconverter.di.components;

import com.kvs.currencyconverter.di.PerActivity;
import com.kvs.currencyconverter.di.modules.ActivityModule;
import com.kvs.currencyconverter.di.modules.ExchangeModule;
import com.kvs.currencyconverter.view.exchange.fragments.ExchangePagerFragment;
import com.kvs.currencyconverter.view.exchange.fragments.ExchangeSelectValueFragment;

import dagger.Component;

/**
 * Created by kvs on 04.07.17.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, ExchangeModule.class})
public interface ExchangeComponent extends ActivityComponent {

    void inject(ExchangePagerFragment exchangePagerFragment);
    void inject(ExchangeSelectValueFragment exchangeSelectValueFragment);
}
