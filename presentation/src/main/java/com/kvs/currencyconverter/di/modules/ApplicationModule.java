package com.kvs.currencyconverter.di.modules;

import android.content.Context;

import com.kvs.currencyconverter.BuildConfig;
import com.kvs.currencyconverter.CurrencyApplication;
import com.kvs.currencyconverter.UIThread;
import com.kvs.data.cache.CurrencyCache;
import com.kvs.data.cache.CurrencyCacheImpl;
import com.kvs.data.errors.RxErrorHandlingCallAdapterFactory;
import com.kvs.data.executor.JobExecutor;
import com.kvs.data.network.FixerService;
import com.kvs.data.repository.CurrencyDataRepository;
import com.kvs.executor.PostExecutionThread;
import com.kvs.executor.ThreadExecutor;
import com.kvs.repository.CurrencyRepository;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kvs on 04.07.17.
 */

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
public class ApplicationModule {
    private final CurrencyApplication application;

    public ApplicationModule(CurrencyApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    CurrencyCache provideUserCache(CurrencyCacheImpl currencyCache) {
        return currencyCache;
    }

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    CurrencyRepository provideUserRepository(CurrencyDataRepository currencyDataRepository) {
        return currencyDataRepository;
    }

    @Provides
    @Singleton
    Bus provideEventBus() {
        return new Bus();
    }

    @Provides
    @Singleton
    FixerService provideFixerApiService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.fixer.io")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())//RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(FixerService.class);
    }
}
