package com.kvs.currencyconverter.di.components;

import android.content.Context;

import com.kvs.currencyconverter.base.activity.BaseActivity;
import com.kvs.currencyconverter.di.modules.ApplicationModule;
import com.kvs.executor.PostExecutionThread;
import com.kvs.executor.ThreadExecutor;
import com.kvs.repository.CurrencyRepository;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by kvs on 04.07.17.
 */

/**
 * A component whose lifetime is the life of the application.
 */
@Singleton //This component one per application
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(BaseActivity baseActivity);

    //Exposed to sub-graphs.
    Context context();
    CurrencyRepository currencyRepository();
    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();
    Bus bus();
}
