package com.kvs.currencyconverter.events;

import com.kvs.currencyconverter.model.ExchangeModel;

/**
 * Created by kvs on 09.07.17.
 */

public class ExchangeModelChangeEvent {

    private ExchangeModel exchangeModel;

    public ExchangeModelChangeEvent(ExchangeModel exchangeModel) {
        this.exchangeModel = exchangeModel;
    }

    public ExchangeModel getExchangeModel() {
        return exchangeModel;
    }
}
