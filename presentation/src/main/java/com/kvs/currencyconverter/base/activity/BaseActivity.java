package com.kvs.currencyconverter.base.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kvs.currencyconverter.CurrencyApplication;
import com.kvs.currencyconverter.R;
import com.kvs.currencyconverter.di.components.ApplicationComponent;
import com.kvs.currencyconverter.di.modules.ActivityModule;
import com.kvs.currencyconverter.navigation.Navigator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kvs on 04.07.17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    protected Navigator navigator;

    @BindView(R.id.toolbar_core)
    Toolbar toolbar;
    @BindView(R.id.txt_toolbar_rates)
    TextView toolbarText;
    @BindView(R.id.view_loading)
    ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_layout);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        this.getApplicationComponent().inject(this);
    }

    public void setToolbarText(String textRates) {
        toolbarText.setText(textRates);
    }

    public void setToolbarTextVisibility(int visibility) {
        toolbarText.setVisibility(visibility);
    }

    public void hideLoading() {
        loading.setVisibility(View.GONE);
    }

    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((CurrencyApplication) getApplication()).getApplicationComponent();
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

}
