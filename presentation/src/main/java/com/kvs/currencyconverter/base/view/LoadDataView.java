package com.kvs.currencyconverter.base.view;

import android.content.Context;

/**
 * Created by kvs on 06.07.17.
 */

public interface LoadDataView {

    void showLoading();

    void hideLoading();

    void showError(String message);

    void retryLoading();

    Context context();

}
