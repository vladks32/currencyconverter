package com.kvs.currencyconverter.base.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kvs on 06.07.17.
 */

public class BaseViewPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentsList = new ArrayList<>();
    private final List<String> mFragmentTitles = new ArrayList<>();

    public BaseViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentsList.add(fragment);
        mFragmentTitles.add(title);
    }

    public String getTitle(int position) {
        return mFragmentTitles.get(position);
    }

    public int getPositionForTitle(String title) {
        for (int i = 0; i < mFragmentTitles.size(); i++) {
                if (mFragmentTitles.get(i).equals(title))
                    return i;
        }
        return 0;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentsList.get(position);
    }

    public List<Fragment> getItems() {
        return mFragmentsList;
    }

    @Override
    public int getCount() {
        return mFragmentsList.size();
    }
}
