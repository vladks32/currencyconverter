package com.kvs.currencyconverter.base.presenter;

/**
 * Created by kvs on 04.07.17.
 */

public interface Presenter {

    void start();

    void resume();

    void pause();

    void stop();

    void destroy();
}
