package com.kvs.currencyconverter.navigation;

import android.support.v7.app.AppCompatActivity;

import com.kvs.currencyconverter.R;
import com.kvs.currencyconverter.view.exchange.fragments.ExchangePagerFragment;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by kvs on 06.07.17.
 */

@Singleton
public class Navigator {

    @Inject
    public Navigator() {}

    public void navigateToAddingCurrencyFragment(AppCompatActivity activity) {

    }

    public void navigateToExchangeFragment(AppCompatActivity activity) {
        if (activity.getSupportFragmentManager().findFragmentByTag(ExchangePagerFragment.TAG) == null) {
            activity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame_core, ExchangePagerFragment.newInstance(), ExchangePagerFragment.TAG)
                    .commit();
        } else {
            activity.onBackPressed();
        }
    }

}
