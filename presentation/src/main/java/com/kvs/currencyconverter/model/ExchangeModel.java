package com.kvs.currencyconverter.model;

/**
 * Created by kvs on 09.07.17.
 */

public class ExchangeModel {

    boolean isBase = false;
    private float valueToExchange;
    private String valueCurrency;

    public String getValueCurrency() {
        return valueCurrency;
    }

    public void setValueCurrency(String valueCurrency) {
        this.valueCurrency = valueCurrency;
    }

    public float getValueToExchange() {
        return valueToExchange;
    }

    public void setValueToExchange(float valueToExchange) {
        this.valueToExchange = valueToExchange;
    }

    public boolean isBase() {
        return isBase;
    }

    public void setBase(boolean base) {
        isBase = base;
    }
}
