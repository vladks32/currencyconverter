package com.kvs.currencyconverter;

import android.app.Application;

import com.kvs.currencyconverter.di.components.ApplicationComponent;
import com.kvs.currencyconverter.di.components.DaggerApplicationComponent;
import com.kvs.currencyconverter.di.modules.ApplicationModule;

/**
 * Created by kvs on 04.07.17.
 */

public class CurrencyApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
