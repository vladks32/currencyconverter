package com.kvs.currencyconverter.view.exchange.views;

import com.kvs.currencyconverter.base.view.LoadDataView;

/**
 * Created by kvs on 07.07.17.
 */

public interface ExchangeSelectValueView extends LoadDataView {

    void initValues();

    void setValue(String value);
}
