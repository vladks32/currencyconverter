package com.kvs.currencyconverter.view.exchange.presenters;

import com.kvs.currencyconverter.base.presenter.Presenter;
import com.kvs.currencyconverter.di.PerActivity;
import com.kvs.currencyconverter.events.ExchangeModelChangeEvent;
import com.kvs.currencyconverter.mapper.CurrenciesModelDataMapper;
import com.kvs.currencyconverter.view.exchange.ExchangeManager;
import com.kvs.currencyconverter.view.exchange.views.ExchangeView;
import com.kvs.data.errors.RetrofitException;
import com.kvs.entity.Currencies;
import com.kvs.interactor.GetCurrencies;
import com.kvs.interactor.GetCurrenciesWithBase;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by kvs on 06.07.17.
 */
@PerActivity
public class ExchangePagerPresenter implements Presenter {

    private String[] actualCurrencies = {"GBP", "EUR", "USD"};
    private final String defaultBase = "EUR";
    private int currenciesToLoadCount = 0;

    private ExchangeView exchangeView;

    private GetCurrencies getCurrencies;
    private GetCurrenciesWithBase getCurrenciesWithBase;
    private CurrenciesModelDataMapper currenciesModelDataMapper; //if should map received to on presentation model
    private Bus bus;

    private ExchangeManager exchangeManager;

    @Inject
    public ExchangePagerPresenter(Bus bus,
                                  GetCurrencies getCurrencies,
                                  GetCurrenciesWithBase getCurrenciesWithBase,
                                  CurrenciesModelDataMapper currenciesModelDataMapper,
                                  ExchangeManager exchangeManager) {
        this.getCurrencies = getCurrencies;
        this.currenciesModelDataMapper = currenciesModelDataMapper;
        this.getCurrenciesWithBase = getCurrenciesWithBase;
        this.exchangeManager = exchangeManager;
        this.bus = bus;
    }

    public void setView(ExchangeView exchangeView) {
        this.exchangeView = exchangeView;
    }

    public void initialize() {
        for (String currency : actualCurrencies) {
            currenciesToLoadCount++;
            loadCurrencies(currency);
        }
    }

    public void initExchangeListener(ExchangeManager.IExchangeListener exchangeListener) {
        exchangeManager.setExchangeListener(exchangeListener);
    }

    private void loadCurrencies(String baseName) {
        exchangeView.showLoading();
        getCurrencies(baseName);
    }

    private void getCurrencies(String baseName) {
        getCurrenciesWithBase.execute(new CurrencyObserver(), GetCurrenciesWithBase.Params.forBaseCurrency(baseName));
    }

    public String[] getActualCurrencies() {
        return actualCurrencies;
    }

    public void selectTopPage(String baseCurrency, String targetCurrency) {
        exchangeManager.resetValues(baseCurrency, targetCurrency);
    }

    public void selectBottomPage(String baseCurrency, String targetCurrency) {
        exchangeManager.calculateTargetValue(baseCurrency, targetCurrency);
    }

    public void newTargetValue(float baseValue, String baseCurrency, String targetCurrency) {
        exchangeManager.calculateTargetValue(baseValue, baseCurrency, targetCurrency);
    }

    public void newBaseValue(float targetValue, String baseCurrency, String targetCurrency) {
        exchangeManager.calculateBaseValue(targetValue, baseCurrency, targetCurrency);
    }

    @Override
    public void start() {
        bus.register(this);
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void stop() {
        bus.unregister(this);
    }

    @Override
    public void destroy() {
        disposeAll();
        exchangeView = null;
    }

    private void disposeAll() {
        getCurrencies.dispose();
        getCurrenciesWithBase.dispose();
    }

    /**
     * Subscribe on events from bus to receives changed values in nested fragments
     **/
    @Subscribe
    public void valueInNestedFragmntChanged(ExchangeModelChangeEvent modelChangeEvent) {
        if (modelChangeEvent.getExchangeModel().isBase()) {
            exchangeView.newEditedBaseValue(modelChangeEvent.getExchangeModel().getValueToExchange(), modelChangeEvent.getExchangeModel().getValueCurrency());
        } else {
            exchangeView.newEditedTargetValue(modelChangeEvent.getExchangeModel().getValueToExchange(), modelChangeEvent.getExchangeModel().getValueCurrency());
        }
    }

    private final class CurrencyObserver extends DisposableObserver<Currencies> {

        @Override
        public void onComplete() {
            exchangeView.hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            if (e instanceof RetrofitException) {
                RetrofitException retrofitException = (RetrofitException) e;
                currenciesToLoadCount = 0;
                exchangeView.showError(retrofitException.getKind() == RetrofitException.Kind.NETWORK ?
                        "Failed network connection" : retrofitException.getMessage());
                exchangeView.retryLoading();
            }
            exchangeView.hideLoading();
        }

        @Override
        public void onNext(Currencies currencies) {
            exchangeManager.putRates(currencies.getBase(), currencies.getRates());
            if (exchangeManager.getRates().size() == currenciesToLoadCount) {
                exchangeView.initPagers(defaultBase);
                currenciesToLoadCount = 0;
                exchangeView.hideLoading();
            }
        }
    }
}
