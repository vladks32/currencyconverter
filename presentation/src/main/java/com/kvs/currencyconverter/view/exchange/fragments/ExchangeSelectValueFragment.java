package com.kvs.currencyconverter.view.exchange.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.kvs.currencyconverter.R;
import com.kvs.currencyconverter.base.fragment.BaseFragment;
import com.kvs.currencyconverter.di.components.ExchangeComponent;
import com.kvs.currencyconverter.view.exchange.presenters.ExchangeSelectValuePresenter;
import com.kvs.currencyconverter.view.exchange.views.ExchangeSelectValueView;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by kvs on 06.07.17.
 */

public class ExchangeSelectValueFragment extends BaseFragment implements ExchangeSelectValueView {

    @Inject
    Bus bus;

    private static final String BUNDLE_CURRENCY = "bundle_curr";
    private static final String BUNDLE_IS_BASE = "bundle_is_base";

    @BindView(R.id.txt_currency)
    TextView textCurrency;
    @BindView(R.id.edit_currency)
    EditText editCurrency;

    private ExchangeSelectValuePresenter presenter;
    private CurrencyValueTextWatcher valueTextWatcher = new CurrencyValueTextWatcher();

    public static ExchangeSelectValueFragment newInstance(String currency, boolean isBase) {
        Bundle args = new Bundle();
        args.putString(BUNDLE_CURRENCY, currency);
        args.putSerializable(BUNDLE_IS_BASE, isBase);
        ExchangeSelectValueFragment fragment = new ExchangeSelectValueFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent(ExchangeComponent.class).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_exchange_select_value, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new ExchangeSelectValuePresenter(
                bus,
                getArguments().getString(BUNDLE_CURRENCY),
                getArguments().getBoolean(BUNDLE_IS_BASE));
        presenter.setView(this);
        presenter.initialize();
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    @Override
    public void showError(String message) {
    }

    @Override
    public void retryLoading() {

    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }

    @Override
    public void initValues() {
        editCurrency.setText("0");
        editCurrency.addTextChangedListener(valueTextWatcher);
        textCurrency.setText(getArguments().getString(BUNDLE_CURRENCY));
    }

    @Override
    public void setValue(String value) {
        if (editCurrency != null && value != null) {
            editCurrency.removeTextChangedListener(valueTextWatcher);
            editCurrency.setText(value);
            editCurrency.addTextChangedListener(valueTextWatcher);
        }
    }

    private class CurrencyValueTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().equals("")) {
                editCurrency.setText("0");
            } else {
                if (charSequence.length() > 1 && charSequence.toString().substring(0, 1).equals("0")) {
                    editCurrency.setText(charSequence.toString().substring(1, charSequence.length()));
                }
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (presenter != null && !editable.toString().equals("")) {
                presenter.valueChanged(editable.toString());
            }
        }
    }
}
