package com.kvs.currencyconverter.view.exchange.views;

import com.kvs.currencyconverter.base.view.LoadDataView;

/**
 * Created by kvs on 06.07.17.
 */

public interface ExchangeView extends LoadDataView {

    void initPagers(String defaultBase);

    void newEditedBaseValue(float value, String currency);

    void newEditedTargetValue(float value, String currency);

}
