package com.kvs.currencyconverter.view;

import android.os.Bundle;
import android.util.Log;

import com.kvs.currencyconverter.base.activity.BaseActivity;
import com.kvs.currencyconverter.di.HasComponent;
import com.kvs.currencyconverter.di.components.DaggerExchangeComponent;
import com.kvs.currencyconverter.di.components.ExchangeComponent;
import com.kvs.currencyconverter.view.exchange.fragments.ExchangePagerFragment;
import com.kvs.entity.Currencies;

import io.reactivex.observers.DisposableObserver;

public class MainActivity extends BaseActivity implements HasComponent<ExchangeComponent> {

    private ExchangeComponent exchangeComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigator.navigateToExchangeFragment(this);
        initializeInjector();
    }

    private void initializeInjector() {
        this.exchangeComponent = DaggerExchangeComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    public ExchangeComponent getComponent() {
        return exchangeComponent;
    }

}
