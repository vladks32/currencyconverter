package com.kvs.currencyconverter.view.exchange.presenters;

import com.kvs.currencyconverter.base.presenter.Presenter;
import com.kvs.currencyconverter.events.ExchangeModelChangeEvent;
import com.kvs.currencyconverter.model.ExchangeModel;
import com.kvs.currencyconverter.view.exchange.views.ExchangeSelectValueView;
import com.squareup.otto.Bus;

/**
 * Created by kvs on 07.07.17.
 */

public class ExchangeSelectValuePresenter implements Presenter {

    private ExchangeSelectValueView exchangeSelectValueView;
    private String currency;
    private boolean isBase = false;
    private Bus bus;

    public ExchangeSelectValuePresenter(Bus bus, String currency, boolean isBase) {
        this.bus = bus;
        this.currency = currency;
        this.isBase = isBase;
    }

    public void setView(ExchangeSelectValueView exchangeSelectValueView) {
        this.exchangeSelectValueView = exchangeSelectValueView;
    }

    public void initialize() {
        exchangeSelectValueView.initValues();
    }

    public void valueChanged(String newValue) {
        ExchangeModel exchangeModel = new ExchangeModel();
        exchangeModel.setBase(isBase);
        exchangeModel.setValueCurrency(currency);
        exchangeModel.setValueToExchange(Float.valueOf(newValue));
        bus.post(new ExchangeModelChangeEvent(exchangeModel));
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
