package com.kvs.currencyconverter.view.exchange.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.kvs.currencyconverter.R;
import com.kvs.currencyconverter.Utils;
import com.kvs.currencyconverter.base.adapters.BaseViewPagerAdapter;
import com.kvs.currencyconverter.base.fragment.BaseFragment;
import com.kvs.currencyconverter.di.components.ExchangeComponent;
import com.kvs.currencyconverter.view.exchange.ExchangeManager;
import com.kvs.currencyconverter.view.exchange.presenters.ExchangePagerPresenter;
import com.kvs.currencyconverter.view.exchange.views.ExchangeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by kvs on 06.07.17.
 */

public class ExchangePagerFragment extends BaseFragment implements ExchangeView, ExchangeManager.IExchangeListener {

    public static final String TAG = ExchangePagerFragment.class.getSimpleName();

    @Inject
    ExchangePagerPresenter exchangePagerPresenter;

    @BindView(R.id.pager_exchange_top)
    ViewPager pagerExchangeTop;
    @BindView(R.id.pager_exchange_bottom)
    ViewPager pagerExchangeBottom;
    @BindView(R.id.indicator_top)
    CircleIndicator indicatorTop;
    @BindView(R.id.indicator_bottom)
    CircleIndicator indicatorBottom;
    @BindView(R.id.retry_loading)
    FrameLayout retryLoading;

    private BaseViewPagerAdapter pagerAdapterTop;
    private BaseViewPagerAdapter pagerAdapterBottom;

    public static ExchangePagerFragment newInstance() {
        return new ExchangePagerFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent(ExchangeComponent.class).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_exchange, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        exchangePagerPresenter.setView(this);
        loadCurrencies();
    }

    @Override
    public void onStart() {
        super.onStart();
        exchangePagerPresenter.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        exchangePagerPresenter.stop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        exchangePagerPresenter.destroy();
    }

    private void loadCurrencies() {
        exchangePagerPresenter.initialize();
    }

    private void initPagerAdapter(String defaultBase) {
        exchangePagerPresenter.initExchangeListener(this);

        pagerAdapterTop = new BaseViewPagerAdapter(getChildFragmentManager());
        pagerAdapterBottom = new BaseViewPagerAdapter(getChildFragmentManager());
        for (String currency : exchangePagerPresenter.getActualCurrencies()) {
            pagerAdapterTop.addFragment(ExchangeSelectValueFragment.newInstance(currency, true), currency);
            pagerAdapterBottom.addFragment(ExchangeSelectValueFragment.newInstance(currency, false), currency);
        }

        pagerExchangeTop.setAdapter(pagerAdapterTop);
        pagerExchangeTop.setCurrentItem(pagerAdapterTop.getPositionForTitle(defaultBase));
        pagerExchangeTop.addOnPageChangeListener(new OnTopPagerChangePageListener());
        indicatorTop.setViewPager(pagerExchangeTop);

        pagerExchangeBottom.setAdapter(pagerAdapterBottom);
        pagerExchangeBottom.addOnPageChangeListener(new OnBottomPagerChangePageListener());
        pagerExchangeBottom.setCurrentItem(pagerExchangeTop.getCurrentItem() + 1);
        indicatorBottom.setViewPager(pagerExchangeBottom);

    }


    @Override
    public void showLoading() {
        getBaseActivity().showLoading();
    }

    @Override
    public void hideLoading() {
        getBaseActivity().hideLoading();
    }

    @Override
    public void showError(String message) {
        showToast(message);
    }

    @Override
    public void retryLoading() {
        retryLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public Context context() {
        return getBaseActivity().getApplicationContext();
    }

    @OnClick(R.id.btn_retry)
    public void retryClick() {
        retryLoading.setVisibility(View.GONE);
        loadCurrencies();
    }

    @Override
    public void initPagers(String defaultBase) {
        initPagerAdapter(defaultBase);
    }

    /**
     * Received values from nested fragment using event bus, because using interface produce NoSerializableException
     **/
    @Override
    public void newEditedBaseValue(float value, String currency) {
        if (currency.equals(pagerAdapterTop.getTitle(pagerExchangeTop.getCurrentItem()))) {
            int bottomPagerItem = pagerExchangeBottom.getCurrentItem();
            exchangePagerPresenter.newTargetValue(value, currency, pagerAdapterBottom.getTitle(bottomPagerItem));
        }
    }

    @Override
    public void newEditedTargetValue(float value, String currency) {
        if (currency.equals(pagerAdapterTop.getTitle(pagerExchangeBottom.getCurrentItem()))) {
            int topPagerItem = pagerExchangeTop.getCurrentItem();
            exchangePagerPresenter.newBaseValue(value, pagerAdapterBottom.getTitle(topPagerItem), currency);
        }
    }

    /**
     * Refresh values in nested fragment
     **/
    @Override
    public void newBaseValue(String value, String forCurrency) {
        ((ExchangeSelectValueFragment) pagerAdapterTop
                .getItem(pagerAdapterTop.getPositionForTitle(forCurrency)))
                .setValue(value);
    }

    @Override
    public void newTargetValue(String value, String forCurrency) {
        ((ExchangeSelectValueFragment) pagerAdapterBottom
                .getItem(pagerAdapterBottom.getPositionForTitle(forCurrency)))
                .setValue(value);
    }

    /**
     * Set rate in toolbar
     **/
    @Override
    public void rate(String baseCurrency, String targetCurrency, float rate) {
        if (rate != 1f) {
            getBaseActivity().setToolbarTextVisibility(View.VISIBLE);
            getBaseActivity().setToolbarText(Utils.formartStringForToolbar(baseCurrency, targetCurrency, String.valueOf(rate)));
        } else {
            getBaseActivity().setToolbarTextVisibility(View.GONE);
        }
    }

    /**
     * ViewPager listeners (bottom and top pagers)
     **/
    private class OnTopPagerChangePageListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            exchangePagerPresenter.selectTopPage(
                    pagerAdapterTop.getTitle(position),
                    pagerAdapterBottom.getTitle(pagerExchangeBottom.getCurrentItem()));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    private class OnBottomPagerChangePageListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            exchangePagerPresenter.selectBottomPage(
                    pagerAdapterTop.getTitle(pagerExchangeTop.getCurrentItem()),
                    pagerAdapterBottom.getTitle(position));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
