package com.kvs.currencyconverter.view.exchange;

import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;

/**
 * Created by kvs on 07.07.17.
 */

public class ExchangeManager {

    public interface IExchangeListener {
        void newBaseValue(String value, String forCurrency);

        void newTargetValue(String value, String forCurrency);

        void rate(String baseCurrency, String targetCurrency, float rate);
    }

    private ConcurrentHashMap<String, HashMap<String, Float>> rates = new ConcurrentHashMap<>();
    private IExchangeListener exchangeListener;
    private float lastBaseValue = 0;


    @Inject
    public ExchangeManager() {
    }

    public ConcurrentHashMap<String, HashMap<String, Float>> getRates() {
        return rates;
    }

    public void calculateTargetValue(float baseValue, String baseCurrency, String targetCurrency) {
        this.lastBaseValue = baseValue;
        if (baseCurrency.equals(targetCurrency)) {
            exchangeListener.rate(baseCurrency, targetCurrency, 1f);
            exchangeListener.newTargetValue(String.valueOf(baseValue), targetCurrency);
        } else {
            exchangeListener.rate(baseCurrency, targetCurrency, rates.get(baseCurrency).get(targetCurrency));
            float targetValue = baseValue * rates.get(baseCurrency).get(targetCurrency);
            exchangeListener.newTargetValue(String.valueOf(targetValue), targetCurrency);
        }
    }

    public void calculateTargetValue(String baseCurrency, String targetCurrency) {
        if (baseCurrency.equals(targetCurrency)) {
            exchangeListener.rate(baseCurrency, targetCurrency, 1f);
            exchangeListener.newTargetValue(String.valueOf(lastBaseValue), targetCurrency);
        } else {
            exchangeListener.rate(baseCurrency, targetCurrency, rates.get(baseCurrency).get(targetCurrency));
            float targetValue = lastBaseValue * rates.get(baseCurrency).get(targetCurrency);
            exchangeListener.newTargetValue(String.valueOf(targetValue), targetCurrency);
        }
    }

    public void calculateBaseValue(float targetValue, String baseCurrency, String targetCurrency) {
        if (targetCurrency.equals(baseCurrency)) {
            this.lastBaseValue = targetValue;
            exchangeListener.rate(baseCurrency, targetCurrency, 1f);
            exchangeListener.newBaseValue(String.valueOf(targetValue), baseCurrency);
        } else {
            exchangeListener.rate(baseCurrency, targetCurrency, rates.get(baseCurrency).get(targetCurrency));
            float baseValue = targetValue / rates.get(baseCurrency).get(targetCurrency);
            this.lastBaseValue = baseValue;
            exchangeListener.newBaseValue(String.valueOf(baseValue), baseCurrency);
        }
    }

    public void putRates(String base, HashMap<String, Float> rates) {
        this.rates.put(base, rates);
    }

    public void resetValues(String baseCurrency, String targetCurrency) {
        if (baseCurrency.equals(targetCurrency)) {
            exchangeListener.rate(baseCurrency, targetCurrency, 1f);
        } else {
            exchangeListener.rate(baseCurrency, targetCurrency, rates.get(baseCurrency).get(targetCurrency));
        }
        this.lastBaseValue = 0;
        exchangeListener.newBaseValue("0", baseCurrency);
        exchangeListener.newTargetValue("0", targetCurrency);
    }

    public void setExchangeListener(IExchangeListener exchangeListener) {
        this.exchangeListener = exchangeListener;
    }
}
