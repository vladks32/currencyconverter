package com.kvs.currencyconverter.mapper;

import com.kvs.currencyconverter.di.PerActivity;
import com.kvs.currencyconverter.model.CurrenciesModel;
import com.kvs.entity.Currencies;

import java.io.Serializable;

import javax.inject.Inject;

/**
 * Created by kvs on 06.07.17.
 */

@PerActivity
public class CurrenciesModelDataMapper implements Serializable {

    @Inject
    CurrenciesModelDataMapper() {
    }

    public CurrenciesModel transform(Currencies currencies) {
        if (currencies == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        CurrenciesModel currenciesModel = new CurrenciesModel();
        currenciesModel.setRates(currencies.getRates());
        currenciesModel.setDate(currencies.getDate());
        currenciesModel.setBase(currencies.getBase());

        return currenciesModel;
    }

}
