package com.kvs.currencyconverter;

/**
 * Created by kvs on 08.07.17.
 */

public class Utils {

    private static String getCurrencySign(String currency) {
        switch (currency) {
            case "USD":
                return "$";
            case "EUR":
                return "€";
            case "GBP":
                return "£";
        }
        return "";
    }

    public static String formartStringForToolbar(String baseCurrency, String targetCurrency, String rate) {
        return getCurrencySign(baseCurrency) + "1 = " + getCurrencySign(targetCurrency) + rate;
    }

}
