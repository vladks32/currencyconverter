package com.kvs.interactor;

import com.kvs.entity.Currencies;
import com.kvs.executor.PostExecutionThread;
import com.kvs.executor.ThreadExecutor;
import com.kvs.repository.CurrencyRepository;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by kvs on 05.07.17.
 */

public class GetCurrenciesWithBase extends UseCase<Currencies, GetCurrenciesWithBase.Params> {

    private final CurrencyRepository currencyRepository;
    private final long pollInterval = 30;

    @Inject
    GetCurrenciesWithBase(CurrencyRepository currencyRepository,
                          ThreadExecutor threadExecutor,
                          PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.currencyRepository = currencyRepository;
    }

    @Override
    Observable<Currencies> buildUseCaseObservable(final Params params) {
        //poll api every 30 seconds
        return Observable.interval(0, pollInterval, TimeUnit.SECONDS).
                flatMap(new Function<Long, Observable<Currencies>>() {
                    @Override
                    public Observable<Currencies> apply(Long aLong) throws Exception {
                        return GetCurrenciesWithBase.this.currencyRepository.currenciesWithBase(params.baseCurrencyName);
                    }
                });
    }

    public static final class Params {
        private final String baseCurrencyName;

        private Params(String baseCurrencyName) {
            this.baseCurrencyName = baseCurrencyName;
        }

        public static Params forBaseCurrency(String baseCurrencyName) {
            return new Params(baseCurrencyName);
        }
    }
}
