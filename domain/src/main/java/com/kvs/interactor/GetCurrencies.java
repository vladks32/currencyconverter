package com.kvs.interactor;

import com.kvs.entity.Currencies;
import com.kvs.executor.PostExecutionThread;
import com.kvs.executor.ThreadExecutor;
import com.kvs.repository.CurrencyRepository;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kvs on 05.07.17.
 */

public class GetCurrencies extends UseCase<Currencies, Void> {

    private final CurrencyRepository currencyRepository;
    private final ThreadExecutor threadExecutor;

    @Inject
    GetCurrencies(CurrencyRepository currencyRepository,
                  ThreadExecutor threadExecutor,
                  PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.currencyRepository = currencyRepository;
        this.threadExecutor = threadExecutor;
    }

    @Override
    Observable<Currencies> buildUseCaseObservable(Void unused) {
        //poll api every 30 seconds
        return Observable.interval(0, 5, TimeUnit.SECONDS).
                flatMap(new Function<Long, Observable<Currencies>>() {
                    @Override
                    public Observable<Currencies> apply(Long aLong) throws Exception {
                        return GetCurrencies.this.currencyRepository.currencies();
                    }
                });
    }
}
