package com.kvs.repository;

import com.kvs.entity.Currencies;

import io.reactivex.Observable;

/**
 * Created by kvs on 05.07.17.
 */

public interface CurrencyRepository {

    Observable<Currencies> currencies();

    Observable<Currencies> currenciesWithBase(final String baseName);

}
