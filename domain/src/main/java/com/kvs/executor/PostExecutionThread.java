package com.kvs.executor;

import io.reactivex.Scheduler;

/**
 * Created by kvs on 06.07.17.
 */

public interface PostExecutionThread {
    Scheduler getScheduler();
}
